init:
	cp .env.dist .env
	vi .env
	cp application/.env.dist application/.env
	vi application/.env
	docker-compose up --build -d
	docker-compose exec apache make init
