<div align="center">
  <img src="./application/public/img/plane.png" height="128">
  <h1>Epreuves de l'étranger</h1>
  <p>
		<b>Une expérience de traduction et de rétrotraduction avec Gérard Macé</b>
	</p>
</div>

## A propos
[http://ouvroir-litt-arts.univ-grenoble-alpes.fr](http://ouvroir-litt-arts.univ-grenoble-alpes.fr/revues/actalittarts/224-l-epreuve-de-l-etranger)

[fabula.org](https://www.fabula.org/actualites/humanites-numeriques-et-texte-litteraire-traduit-editer-et-analyser-des-corpus-de-versions_91930.php)

## Installation
```
git clone https://gitlab.com/litt-arts-num/epreuves_de_l_etranger.git
cd epreuves_de_l_etranger
make init
// et pour l'instant il faut importer dblst.sql manuellement
```

## Licence
[MIT](LICENSE)
