const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export var documentHandler = {
  display: function(docId) {
    const url = Routing.generate('document_render', {
      id: docId
    })
    $.ajax({
      method: 'POST',
      url: url,
    }).done((data) => {
      $("#documents").append(data)
      checkMoveBtns()
      addClickEvent()
      checkLinks()
    })

    return
  },

  export: function() {
    let documents = document.querySelectorAll('[data-doc-id]')
    let ids = ""
    Array.prototype.forEach.call(documents, function(document) {
      ids += document.getAttribute('data-doc-id') + "-"
    })

    let url = Routing.generate('export', {
      docIds: ids
    })

    window.location = url
  }
}

function getDocument(doc) {
  let docId = doc.getAttribute('data-id')
  documentHandler.display(docId)
}

function removeDocument(doc) {
  doc.closest('.document-container').remove();
  checkMoveBtns()
  checkLinks()

  return
}

function hideAll() {
  document.getElementById("documents").innerHTML = ""
  checkLinks()
}

function addClickEvent() {
  let segs = document.getElementsByTagName('seg')
  Array.prototype.forEach.call(segs, function(seg) {
    seg.addEventListener("mouseenter", function() {
      seg.classList.add("matching-seg")
      let id = seg.hasAttribute("xml:id") ? "#" + seg.getAttribute("xml:id") : seg.getAttribute("corresp")
      let corresps = document.querySelectorAll('[corresp="' + id + '"], [xml\\:id="' + id.substring(1) + '"]')
      Array.prototype.forEach.call(corresps, function(corresp) {
        corresp.classList.add("matching-seg")
      });
    });

    seg.addEventListener("mouseleave", function() {
      seg.classList.remove("matching-seg")
      let id = seg.hasAttribute("xml:id") ? "#" + seg.getAttribute("xml:id") : seg.getAttribute("corresp")
      let corresps = document.querySelectorAll('seg[corresp="' + id + '"], seg[xml\\:id="' + id.substring(1) + '"]')
      Array.prototype.forEach.call(corresps, function(corresp) {
        corresp.classList.remove("matching-seg")
      });
    });
  });

  let leftLinks = document.getElementsByClassName('left-btn');
  Array.prototype.forEach.call(leftLinks, function(link) {
    if (!link.classList.contains('disabled')) {
      let linkClone = link.cloneNode(true);
      link.parentNode.replaceChild(linkClone, link);
      linkClone.addEventListener("click", function() {
        move("left", this)
      });
    }
  });

  let rightLinks = document.getElementsByClassName('right-btn')
  Array.prototype.forEach.call(rightLinks, function(link) {
    if (!link.classList.contains('disabled')) {
      let linkClone = link.cloneNode(true)
      link.parentNode.replaceChild(linkClone, link)
      linkClone.addEventListener("click", function() {
        move("right", this)
      })
    }
  })

  let addLinks = document.getElementsByClassName('display-doc-btn')
  Array.prototype.forEach.call(addLinks, function(link) {
    let linkClone = link.cloneNode(true)
    let id = link.getAttribute('data-id')
    let docs = document.querySelector("[data-doc-id='" + id + "']")
    if (docs) {
      link.classList.add("disabled")
    }

    link.parentNode.replaceChild(linkClone, link)
    linkClone.addEventListener("click", function() {
      getDocument(this)
    })
  });

  let deleteLinks = document.getElementsByClassName('delete-doc-btn')
  Array.prototype.forEach.call(deleteLinks, function(link) {
    let linkClone = link.cloneNode(true)
    link.parentNode.replaceChild(linkClone, link)

    linkClone.addEventListener("click", function() {
      removeDocument(this)
    });
  });
}

function move(to, doc) {
  var container = doc.closest('.document-container')
  var container2 = (to == "left") ? container.previousElementSibling : container.nextElementSibling

  if (container2) {
    var parent = container2.parentNode

    var containerClone = container.cloneNode(true)
    var container2Clone = container2.cloneNode(true)

    parent.replaceChild(container2Clone, container)
    parent.replaceChild(containerClone, container2)

    checkMoveBtns()
    addClickEvent()
    checkLinks()
  }
}

function checkMoveBtns() {
  let leftLinks = document.getElementsByClassName('left-btn');
  Array.prototype.forEach.call(leftLinks, function(link) {
    var container = link.closest('.document-container')
    var leftContainer = container.previousElementSibling
    if (!leftContainer) {
      link.classList.add('disabled')
    } else {
      link.classList.remove('disabled')
    }
  });

  let rightLinks = document.getElementsByClassName('right-btn');
  Array.prototype.forEach.call(rightLinks, function(link) {
    var container = link.closest('.document-container')
    var rightContainer = container.nextElementSibling
    if (!rightContainer) {
      link.classList.add('disabled')
    } else {
      link.classList.remove('disabled')
    }
  });
}

function checkLinks() {
  let addLinks = document.getElementsByClassName('display-doc-btn')

  Array.prototype.forEach.call(addLinks, function(link) {
    let id = link.getAttribute('data-id')
    let docs = document.querySelector("[data-doc-id='" + id + "']")
    if (docs) {
      link.classList.add("disabled")
    } else {
      link.classList.remove("disabled")
    }
  });
}

$(document).ready(function() {
  checkMoveBtns()
  addClickEvent()
  checkLinks()

  document.getElementById('hide-all').addEventListener("click", hideAll)
  document.getElementById('export').addEventListener("click", documentHandler.export)
});