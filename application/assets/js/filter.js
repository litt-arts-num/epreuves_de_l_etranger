let input = document.getElementById("filter")

input.addEventListener("keyup", function () {
  filter(this.value)
}, false);

function filter(value){
  let entries = document.getElementsByClassName("document-list-item");
  let entriesArray = Array.from(entries)

  entriesArray.forEach(function(entry) {
      let cellContent = entry.innerHTML
      entry.style.display = (cellContent.indexOf(value) > -1)
        ? "block"
        : "none"
  });
}
