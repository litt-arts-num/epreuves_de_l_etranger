import cytoscape from "cytoscape"
import fcose from 'cytoscape-fcose'
import layoutUtilities from 'cytoscape-layout-utilities'
import {
  documentHandler
} from './document.js'

cytoscape.use(fcose)
cytoscape.use(layoutUtilities)

document.addEventListener('DOMContentLoaded', function() {
  var containerCy = document.getElementById('cy')
  var dataJSON = JSON.parse(containerCy.dataset.datas)
  let options = {
    name: 'fcose',
    animate: true,
    animationEasing: 'ease-out',
    edgeElasticity: 1,
    fit: true,
    idealEdgeLength: 80,
    initialEnergyOnIncremental: 1,
    nestingFactor: 1,
    nodeRepulsion: 299999,
    nodeSeparation: 275,
    numIter: 4500,
    packComponents: true,
    quality: "proof",
    randomize: true,
    tilingPaddingHorizontal: 100,
    tilingPaddingVertical: 100,
    uniformNodeDimensions: true,
  }
  var cy = cytoscape({
    container: containerCy,
    ready: function() {
      this.layout(options).run();
    },
    elements: dataJSON,
    style: [{
        selector: 'node',
        style: {
          'background-color': 'data(color)',
          'label': 'data(text)',
          'width': 'data(weight)',
          'height': 'data(weight)'
        }
      },
      {
        selector: 'edge',
        style: {
          'width': 2,
          'line-color': 'grey',
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle',
          'target-arrow-color': 'grey',
        }
      }
    ],
  });

  cy.on('click', 'node', function(evt) {
    var node = evt.target;
    documentHandler.display(node.id())
    Toastr.info("Document ajouté")
  });

  cy.on('click', 'edge', function(evt) {
    var edge = evt.target;
  });
});