<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\User;
use App\Form\DocumentType;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }

    /**
     * @Route("/documents", name="list_documents")
     */
    public function listDocuments()
    {
        $documents = $this->getDoctrine()->getRepository(Document::class)->findAll();

        return $this->render('document/index.html.twig', [
          "documents" => $documents
        ]);
    }

    /**
     * @Route("/document/edit/{id}", name="document_edit", defaults={"id"= null})
     */
    public function editDocument(Document $document = null, Request $request)
    {
        if (!$document) {
            $document = new Document;
        }

        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            $this->addFlash('notice', 'document sauvegardé');

            return $this->redirectToRoute("admin_list_documents");
        }


        return $this->render('document/edit.html.twig', [
          "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/user/switch/{id}", name="switch_admin")
     */
    public function switchUser(User $user, UserManager $um)
    {
        $um->switchAdmin($user);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }
}
