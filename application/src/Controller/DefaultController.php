<?php

namespace App\Controller;

use App\Entity\Document;
use App\Manager\DataManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(DataManager $dm)
    {
        $baseDoc = $this->getDoctrine()->getRepository(Document::class)->findOneBySource(null);
        $docs = $this->getDoctrine()->getRepository(Document::class)->findAll();
        $datas = $dm->getJsonData($baseDoc);

        return $this->render('default/index.html.twig', [
          'baseDoc' => $baseDoc,
          'docs' => $docs,
          'datas' => $datas
        ]);
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/montage-sonore", name="montage")
     */
    public function montageSonore()
    {
        return $this->render('default/montage-sonore.html.twig');
    }
}
