<?php

namespace App\Controller;

use App\Entity\Document;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DocumentController extends AbstractController
{
    /**
     * @Route("/document/{id}/render", name="document_render", options={"expose"=true})
     */
    public function display(Document $document)
    {
        return $this->render('document/display.html.twig', [
            'doc' => $document
        ]);
    }

    /**
     * @Route("/export/{docIds}", name="export", options={"expose"=true})
     */
    public function export($docIds)
    {
        $ids = preg_split('/-/', $docIds, null, PREG_SPLIT_NO_EMPTY);
        var_dump($ids);
        $documents = [];
        foreach ($ids as $id) {
            $documents[] = $this->getDoctrine()->getRepository(Document::class)->findOneById($id);
        }

        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($pdfOptions);
        $html = $this->renderView('export/pdf.html.twig', [
              'documents' => $documents
          ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $dompdf->stream("pdf-export", [
              "Attachment" => true
          ]);
    }
}
