<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('title', TextType::class, [
            'label' => "Titre",
          ])
          ->add('source', EntityType::class, [
            'required' => false,
            'placeholder' => '-----------------',
            'class' => 'App:Document',
            'choice_label' => function ($document) {
                return $document->getTitle() . ' : ' . $document->getLanguage()->getName();
            },
            'label' => "Source",
            'multiple'  => false,
          ])
          ->add('language', EntityType::class, [
            'class' => 'App:Language',
            'choice_label' => 'name',
            'label' => "Langue",
            'multiple'  => false,
          ])
          ->add('content', TextareaType::class, [
            'label' => "Contenu",
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'Sauvegarder',
          ));
    }
}
