<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Document;

class DataManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getJsonData(Document $baseDoc = null)
    {
        $datas = [];
        if ($baseDoc) {
          $baseId = $baseDoc->getId();
          $this->createNode($baseId, $baseDoc->getTitle(), "green", 30, $datas);
          foreach ($baseDoc->getTranslations() as $translation) {
              $translId = $translation->getId();
              $edgeId = "edge_".$baseId."-".$translId;
              $label = $translation->getLanguage()->getName()." ".substr($translation->getTitle(), -1);
              $this->createNode($translId, $label, "blue", 30, $datas);
              $this->createEdge($edgeId, $translId, $baseId, 30, $datas);

              foreach ($translation->getTranslations() as $retroTrad) {
                  $retroTradId = $retroTrad->getId();
                  $edgeRetroId = "edge_".$translId."-".$retroTradId;
                  $label = $retroTrad->getLanguage()->getName();
                  $this->createNode($retroTradId, $label, "red", 30, $datas);
                  $this->createEdge($edgeRetroId, $retroTradId, $translId, 30, $datas);
              }
           }
        }

        return json_encode($datas);
    }

    private function createNode($id, $text, $color, $weight, &$datas)
    {
        $datas[] = ['group' => "nodes", "data" => ["weight"=> $weight, "color"=>$color, "id" => $id, "text" =>  $text ]];

        return;
    }

    private function createEdge($id, $target, $source, $weight, &$datas)
    {
        $datas[] = ['group' => "edges", "data" => ["source" => $source, "target" => $target, "weight"=> $weight, "id" => $id ]];

        return;
    }
}
