var Encore = require('@symfony/webpack-encore');

Encore
  // directory where compiled assets will be stored
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .addEntry('js/main', './assets/js/main.js')
  .addEntry('js/filter', './assets/js/filter.js')
  .addEntry('js/document', './assets/js/document.js')
  .addEntry('js/graph', './assets/js/graph.js')
  .addStyleEntry('css/template', './assets/css/template.css')
  .addStyleEntry('css/project', './assets/css/project.css')
  .addStyleEntry('css/toastr', './node_modules/toastr/build/toastr.min.css')
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning()
  .createSharedEntry('js/vendor', './assets/js/vendor.js')
  .autoProvidejQuery()
  .autoProvideVariables({
    Toastr: 'toastr',
  });

module.exports = Encore.getWebpackConfig();